<?php

/**
 * @file
 * Database schema code for MySQL database servers.
 */

include_once DRUPAL_ROOT . '/includes/database/mysql/schema.inc';

/**
 * @ingroup schemaapi
 * @{
 */

class DatabaseSchema_mongo extends DatabaseSchema_mysql {
}

/**
 * @} End of "ingroup schemaapi".
 */
