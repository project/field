<?php

/**
 * @file
 * Installation code for MongoDB.
 */


// MongoDB specific install functions

class DatabaseTasks_mongo extends DatabaseTasks {
  protected $pdoDriver = 'mysql';
  public function name() {
    return 'Mongo';
  }
}
