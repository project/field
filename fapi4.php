<?php

abstract class element implements ArrayAccess {
  protected $properties;
  function __construct($properties = array()) {
    $this->properties = $properties;
  }
  function offsetExists($offset) {
    return isset($this->properties[$offset]) || array_key_exists($offset, $this->properties);
  }
  function offsetGet($offset) {
    return isset($this->properties[$offset]) ? $this->properties[$offset] : NULL;
  }
  function offsetSet($offset, $value) {
    $this->properties[$offset] = $value;
  }
  function offsetUnset($offset) {
    unset($this->properties[$offset]);
  }
}

class form extends element {
  function __construct($properties = array()) {
    $properties += array('action' => $_GET['q']);
    parent::__construct($properties);
  }
}

class textfield extends element {
}

$_GET['q'] = 'node/1/edit';
$form = new form(array('redirect' => 'homepage'));
$form->child1 = new textfield(array(
  'default_value' => 'test',
));;
echo $form['action'];
echo "\n";
// Change action.
$form['action'] = 'this is another action';
echo $form['action'];
echo "\n";
foreach ($form as $k => $v) {
  echo "$k:\n";
  var_dump($v);
}
echo "\n";
echo serialize($form);
