<?php

function invite_menu($may_cache) {
  global $user;
  $items = array();
  if (!$may_cache) {
    $items[] = array('path' => 'invite', 'access' => TRUE, 'type' => MENU_CALLBACK, 'title' => '', 'callback' => 'invite_page');
    $items[] = array('path' => 'invite_move', 'access' => $user->uid, 'type' => MENU_CALLBACK, 'title' => '', 'callback' => 'invite_move');
    $items[] = array('path' => 'invite_user', 'access' => $user->uid, 'title' => t('invite user'), 'callback' => 'invite_user_page');
  }
  return $items;
}

/**
 * This function lets you send invites. You may invite someone to join the site,
 * a group, an event etc.
 *
 * @param $recipient
 *   An email address or a uid to be invited.
 * @param $subject
 *   the subject of the mail to be sent.
 * @param $message
 *   the body of the mail to be sent.
 * @param $callbacks
 *   array of arrays. Each array has the following pairs:
 *    'type' A string describing the type of invite. 'user' is used
 *           to invite someone who is not registered.
 *    'accept callback' The name of the callback to call upon accept.
 *    'deny callback' An optional name for the callback to call upon deny.
 *   'callback arguments' The callbacks two parameters are always the uid
 *                        of the inviter and the invited and then comes this
 *                         optional array
 * @param $timeout
 *   timeout in seconds, defaults to 630720000
 * @return
 *   errorstring on fault
**/
function invite_send($recipient, $subject, $message, $callbacks, $timeout = 630720000) {
  global $user, $base_url;
  $timeout = $timeout ? $timeout : 630720000;
  foreach ($callbacks as $callback) {
    if (!isset($callback["callback arguments"])) {
      $callback["callback arguments"] = array();
    }
  }
  $time = time() + $timeout;
  $iid_key = user_password();
  $iid = db_next_id('{invite}_iid');
  if (!is_numeric($recipient)) {
    if (!$uid = db_result(db_query("SELECT uid FROM {users} WHERE LOWER(mail) = LOWER('%s')", $recipient))) {
      if ($error = _invite_validate_mail($recipient)) {
        return $error;
      }
      $account = _invite_external($recipient, $subject, $message, $iid, $iid_key);
      $mailed = 1;
      $recipient = $account->uid;
      if ($callbacks[0]['type'] != 'user') {
        array_unshift($callbacks, array('accept callback' => 'invite_user_form', 'type' => 'user'));
      }
    }
    else {
      $recipient = $uid;
    }
  }
  if (!isset($account)) {
    $account = user_load(array('uid' => $recipient));
  }
  $link = $base_url .'/invite/'. $recipient .'/'. $iid .'/'. $iid_key;
  $message = str_replace('%accept-link', $link .'/1', $message);
  $message = str_replace('%deny-link', $link .'/0', $message);
  db_query("INSERT INTO {invite} (iid, uid1, uid2, callbacks, timeout, active, iid_key) VALUES (%d, %d, %d, '%s', %d, 1, '%s')", $iid, $user->uid, $recipient, serialize($callbacks), $time, $iid_key);
  if (!$mailed) {
    invite_mail($recipient, $subject, $message);
  }
}

function invite_mail($uid, $subject, $message) {
  $account = user_load(array('uid' => $uid));
  $from = variable_get('site_mail', ini_get('sendmail_from'));
  return user_mail($account->mail, $subject, $message, "From: $from\nReply-to: $from\nX-Mailer: Drupal\nReturn-path: $from\nErrors-to: $from");
}

function _invite_validate_mail($mail) {
  if ($error = user_validate_mail($mail)) {
    return $error;
  }
  else if (db_result(db_query("SELECT uid FROM {users} WHERE LOWER(mail) = LOWER('%s')", $mail))) {
    return t('The e-mail address %email is already taken.', array('%email' => theme('placeholder', $mail)));
  }
  else if (user_deny('mail', $mail)) {
    return t('The e-mail address %email has been denied access.', array('%email' => theme('placeholder', $mail)));
  }
}

function _invite_external($mail, $subject, $message, $iid, $iid_key) {
  global $user, $base_url;
  $pass = user_password();
  $account = user_save('', array('name' => $mail, 'pass' => $pass, 'init' => $mail, 'roles' => array(_user_authenticated_id()), 'login' => 0, 'status' => 0, 'mail' => $mail));
  $link = $base_url .'/invite/'. $account->uid .'/'.  $iid .'/'. $iid_key;
  $message = str_replace('%accept-link', $link .'/1', $message);
  $message = str_replace('%deny-link', $link .'/0', $message);
  if (invite_mail($account->uid, $subject, $message)) {
    watchdog('user', t('%user invited %mail', array('%user' => $user->name, '%mail' => $account->mail)));
  }
  return $account;
}

function invite_cron() {
  $time = time();
  $result = db_query('SELECT * FROM {invite} WHERE timeout < %d', $time);
  while ($invite = db_fetch_object($result)) {
    $callbacks = unserialize($invite->callbacks);
    $types = array();
    foreach ($callbacks as $callback) {
      if (!empty($callback["deny callback"])) {
        $arguments = array_merge(array($invite->uid1, $invite->uid2), $callback['callback arguments']);
        call_user_func($callback['deny callback'], $arguments);
      }
      $types[] = $callback["type"];
    }
    if ($invite->uid1 != $invite->uid2) {
      $account = user_load($invite->uid2);
      invite_mail($invite->uid1, t('Invitation result'), t('Invitation to %name of type %type has expired.', array('%name' => $account->name, '%type' => implode(",", $types))));
    }
  }
  db_query("UPDATE {invite} SET timeout = 0 WHERE timeout < %d", $time);
}

function invite_page($uid, $iid, $iid_key, $accept) {
  global $user;
  if ($invite = db_fetch_object(db_query("SELECT * FROM {invite} WHERE iid = %d AND iid_key ='%s' AND timeout > %d", $iid, $iid_key, time()))) {
    $callbacks = unserialize($invite->callbacks);
    $callback = array_shift($callbacks);
    if ($callback['accept callback'] == 'invite_user_form') {
      if ($user->uid) {
        drupal_set_message(t('You are logged in as %name. If you are not %name then please use the register form below. If you want to continue as %name please click %here', array('%name' => $user->name, '%here' => l(t('here'), "invite_move/$user->uid/$iid/$iid_key/$accept"))));
      }
    }
    elseif ($uid != $user->uid) {
      drupal_access_denied();
      die();
    }
    $callback_func = $accept ? $callback["accept callback"] : $callback["deny callback"];
    if (function_exists($callback_func)) {
      $arguments = array_merge(array($invite->uid1, $invite->uid2), $callback["callback arguments"]);
      $return = call_user_func_array($callback_func, $arguments);
      if ($return == array($invite->uid1, $invite->uid2)) {
        invite_next();
      }
      else {
        return $return;
      }
    }
  }
  else {
    drupal_access_denied();
  }
}

function invite_in_progress($uid, $type) {
  global $user;
  return db_num_rows(db_query("SELECT * FROM {invite} WHERE callbacks LIKE '%%s:4:\"type\";s:%d:\"%s\";%%' AND uid1 = %d AND uid2 = %d AND timeout > %d", strlen($type), $type, $user->uid, $uid, time()));
}

function invite_move($uid, $iid, $iid_key, $accept) {
  global $user;
  if ($invite = db_fetch_object(db_query("SELECT * FROM {invite} WHERE iid = %d AND iid_key ='%s' AND timeout > %d", $iid, $iid_key, time()))) {
    $callbacks = unserialize($invite->callbacks);
    $callback = array_shift($callbacks);
    if ($callback['accept callback'] == 'invite_user_form') {
      db_query('UPDATE {invite} SET uid2 = %d WHERE iid = %d', $user->uid, $iid);
      invite_next();
    }
  }
}

function invite_next() {
  $invite = db_fetch_object(db_query('SELECT * FROM {invite} WHERE iid = %d ', arg(2)));
  $callbacks = unserialize($invite->callbacks);
  $callback = array_shift($callbacks);
  db_query("UPDATE {invite} SET callbacks = '%s' WHERE iid = %d", serialize($callbacks), $invite->iid);
  if (empty($callbacks)) {
    if ($invite->uid1 != $invite->uid2) {
      $account = user_load(array('uid' => $invite->uid2));
      if (arg(4)) {
        invite_mail($invite->uid1, t('Invitation result'), t('Invitation to %name of type %type is accepted.', array('%name' => $account->name, '%type' => $callback['type'])));
      }
      else {
        invite_mail($invite->uid1, t('Invitation result'), t('Invitation(s) to %name of type %type is rejected.', array('%name' => $account->name, '%type' => $callback['type'])));
      }
    }
    drupal_goto();
  }
  else {
    drupal_goto(str_replace('invite_move', 'invite', $_GET['q']));
  }
}

function invite_user_form($inviter_uid, $uid) {
  global $user;
  drupal_set_title(t('Register'));
  if (isset($_POST['op']) && $_POST['op'] == t('Create new account')) {
    $edit = $_POST['edit'];
    $category = 'account';
    $account = user_load(array('uid' => $uid));
    $edit['mail'] = $account->mail;
    if (empty($edit['name'])) {
      form_set_error('name', t('User name can not be empty'));
    }
    if (empty($edit['pass1'])) {
      form_set_error('pass1', t('You must choose a password'));
    }
    user_module_invoke('validate', $edit, $account, $category);
    if (!form_get_errors()) {
      // Validate input to ensure that non-privileged users can't alter protected data.
      if (!user_access('administer users') && array_intersect(array_keys($edit), array('uid', 'roles', 'init', 'session'))) {
        watchdog('security', t('Detected malicious attempt to alter protected user fields.'), WATCHDOG_WARNING);
      }
      else {
        user_save($account, $edit, $category);
        // Delete that user's menu cache.
        cache_clear_all('menu:'. $account->uid, TRUE);
        drupal_set_message(t('The changes have been saved.'));
        db_query('UPDATE {users} SET login = %d, status = 1 WHERE uid = %d', time(), $account->uid);
        $user = $account;
        user_module_invoke('login', $edit, $user);
        return array($inviter_uid, $uid);
      }
    }
  }
  $output = variable_get('user_registration_help', '');
  $default = form_textfield(t('Username'), 'name', $edit['name'], 30, 64, t('Your full name or your preferred username; only letters, numbers and spaces are allowed.'), NULL, TRUE);
  $default .= form_item(t('Password'), '<input type="password" class="form-password" name="edit[pass1]" size="12" maxlength="24" /> <input type="password" class="form-password" name="edit[pass2]" size="12" maxlength="24" />', t('Enter your new password twice.'), NULL, TRUE);
  $extra = _user_forms($edit, NULL, $category, 'register');
  // Only display form_group around default fields if there are other groups.
  if ($extra) {
    $form .= form_group(t('Account information'), $default);
    $form .= $extra;
  }
  else {
    $form .= $default;
  }
  $form .= form_submit(t('Create new account'));
  $output .= form($form);
  print theme('page', $output);
}

function invite_user_page() {
  global $user;
  if (isset($_POST['op']) && $_POST['op'] == t('Submit')) {
    $edit = $_POST['edit'];
    if (empty($edit['message'])) {
      form_set_error('message', t('The message can not be empty'));
      return;
    }
    $callbacks = array(array('accept callback' => 'invite_user_form', 'type' => 'user'));
    $message = variable_get('invite_user_message', t('%user has invited you to %sitename. Click on this %accept-link to create an account. His message is: %message', array('%user' => $user->name, '%sitename' => variable_get('site_name', 'drupal'), '%message' => $edit['message'])));
    $error = invite_send($edit['mail'], t('Invitation to join %sitename', array('%sitename' => variable_get('site_name', 'drupal'))), $message, $callbacks, 86400);
    if (!empty($error)) {
      form_set_error('mail', $error);
    }
    else {
      drupal_set_message(t('Invitation sent to %mail', array('mail' => $edit['mail'])));
    }
    drupal_goto();
  }
  $form = form_textfield(t('E-mail address'), 'mail', $edit['mail'], 30, 64);
  $form .= form_textarea(t('Invite message'), 'message', $message, 60, 20);
  $form .= form_submit(t('Submit'));
  $form .= form_hidden('destination', $_GET['q']);
  print theme('page', form($form));
}

function invite_settings() {
  return form_textfield(t('Invite user to register message'), 'invite_user_message', variable_get('invite_user_message', t('%user has invited you to %sitename. Click on this %accept-link to create an account. His message is: %message', array('%user' => $user->name, '%sitename' => variable_get('site_name', 'drupal'), '%message' => $edit['message']))), 60, 20);
}

?>