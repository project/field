<?php

class sphinxViewsQuery extends views_plugin_query {

  var $args = array();
  var $method = '';

  function init($table, $field) {
    include_once 'sphinxapi.php';
    $this->sphinx = _sphinx_views_connect();
  }

  function add_field($table, $field, $alias = '', $params = NULL) {
    return $field;
  }

  function add_orderby($table, $field, $order, $alias = '') {
    $this->sphinx->SetSortMode(constant('SPH_SORT_ATTR_'. strtoupper($order)), $field);
  }

  function ensure_table() {}

  /**
   * Execute a call to flickr.
   */
  function execute(&$view) {

    // What page was requested:
    $pager_page_array = isset($_GET['page']) ? explode(',', $_GET['page']) : array();
    if (!empty($pager_page_array[$view->pager['element']])) {
      $page = intval($pager_page_array[$view->pager['element']]);
    }
    $per_page = empty($view->pager['items_per_page']) ? 500 : $view->pager['items_per_page'];
    $this->sphinx->SetLimits($page * $per_page, $per_page);
    $results = $this->sphinx->Query('', $this->sphinx->index);
    foreach ($results['matches'] as $id => $match) {
      $view->result[] = (object)$match['attrs'];
    }

    if (!empty($view->pager['items_per_page'])) {
      // We no longer use pager_query() here because pager_query() does not
      // support an offset. This is fine as we don't actually need pager
      // query; we've already been doing most of what it does, and we
      // just need to do a little more playing with globals.
      if (!empty($view->pager['use_pager']) || !empty($view->get_total_rows)) {
        // We use total because that's the number that actually can return
        // as capped by max_matches. total_found is the number of real results.
        $view->total_rows = empty($results['total']) ? 0 : $results['total'];
      }

      if (!empty($view->pager['use_pager'])) {
        // dump information about what we already know into the globals
        global $pager_page_array, $pager_total, $pager_total_items;
        // total rows in query
        $pager_total_items[$view->pager['element']] = $view->total_rows;
        // total pages
        $pager_total[$view->pager['element']] = intval($view->total_rows / $per_page) + 1;

        // If the requested page was within range. $view->pager['current_page']
        // defaults to 0 so we don't need to set it in an out-of-range condition.
        if (isset($page)) {
          if ($page > 0 && $page < $pager_total[$view->pager['element']]) {
            $view->pager['current_page'] = $page;
          }
        }
        $pager_page_array[$view->pager['element']] = $view->pager['current_page'];
      }
    }
  }
}