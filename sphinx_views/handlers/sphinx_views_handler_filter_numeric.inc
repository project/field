<?php

class sphinx_views_handler_filter_numeric extends views_handler_filter_numeric {
  function op_simple($field) {
    // Hack: we know it starts with a dot.
    $field = substr($field, 1);
    $value = $this->value['value'];
    _sphinx_views_op_simple($this->query->sphinx, $field, $value, $this->operator);
  }
  
  function op_between($field) {
    // Hack: we know it starts with a dot.
    $field = substr($field, 1);
    $this->query->sphinx->setFilterRange($field, $this->value['min'], $this->value['max'], $this->operator != 'between');
  }
}