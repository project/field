<?php

class sphinx_views_handler_filter_date extends views_handler_filter_numeric {
  function op_simple($field) {
    // Hack: we know it starts with a dot.
    $field = substr($field, 1);
    $value = strtotime($this->value['value']);
    _sphinx_views_op_simple($this->query->sphinx, $field, $value, $this->operator);
  }
  function op_between($field) {
    // Hack: we know it starts with a dot.
    $field = substr($field, 1);
    $min = strtotime($this->value['min']);
    $max = strtotime($this->value['max']);
    $this->query->sphinx->setFilterRange($field, $min, $max, $this->operator != 'between');
  }
}