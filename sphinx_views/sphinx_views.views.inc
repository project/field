<?php

// Implementation of hook_views_data
function sphinx_views_views_data() {
  $sphinx = _sphinx_views_connect();
  $sphinx->SetLimits(0, 1);
  $results = $sphinx->Query('', $sphinx->index);
  $data['sphinx']['table']['group']  = 'Sphinx';
  $data['sphinx']['table']['base'] = array(
    'title' => 'Sphinx',
    'help' => t('Results from Sphinx'),
    'query class' => 'sphinxViewsQuery'
  );
  $map = array(1 => 'numeric', 2 => 'date');
  foreach ($results['attrs'] as $attr => $type) {
    $postfix = $map[$type];
    $data['sphinx'][$attr] = array(
      'title' => $attr,
      'help' => $attr,
      'field' => array(
        'click sortable' => TRUE,
        'handler' => 'views_handler_field_'. $postfix,
      ),
      'filter' => array(
        'handler' => 'sphinx_views_handler_filter_'. $postfix,
      ),

    );
  }
  return $data;
}
