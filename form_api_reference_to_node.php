<?php

require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
$f = file_get_contents('/tmp/docs/topics/forms_api_reference.html');
preg_match_all('|>([^>]+)</h3>|', $f, $matches);
$names = $matches[1];
require_once './modules/node/node.pages.inc';
$nids = array();
foreach ($names as $name) {
  if (!$nid = db_result(db_query("SELECT nid FROM {node} WHERE title = '%s'", $name))) {
    $form_state = array('values' => array('title' => $name, 'op' => t('Save')));
    drupal_execute('fapi_reference_node_form', $form_state, array('type' => 'fapi_reference', 'name' => ''));
    $nid = $form_state['nid'];
  }
  $nids[$name] = $nid;
}
$sections = explode('<h3>', $f);
$first = array_shift($sections);
foreach ($sections as $key => $section) {
  $name = $names[$key];
  $node = node_load($nids[$name]);
  $values = array();
  $values['op'] = t('Save');
  preg_match('|<p><strong>Description</strong>: (.+).</p>|U', $section, $matches);
  $values['body'] = $matches[1];
  preg_match_all('|(#?[a-z_]+)</a>|', $section, $matches);
  foreach ($matches[1] as $property) {
    if (isset($nids[$property])) {
      $values['field_properties']['nid']['nid'][$nids[$property]] = $nids[$property];
    }
  }
  if (isset($values['field_properties'])) echo "$node->nid<br>";
  if (preg_match('|<code>(.*)</code>|s', $section, $matches)) {
    $code_example = preg_replace("|<br />\n*|", "\n", $matches[1]);
    $code_example = preg_replace('#</?span( class="[^"]+")?>#U', '', $code_example);
    $values['field_code_example'][0]['value'] = html_entity_decode($code_example, ENT_QUOTES, 'UTF-8');
  }
  $form_state = array('values' =>  $values);
  drupal_execute('fapi_reference_node_form', $form_state, (array)$node);
}
echo 'done';
/**
$content[type]  = array (
  'name' => 'fapi reference',
  'type' => 'fapi_reference',
  'description' => 'A property or an element type',
  'title_label' => 'Name',
  'body_label' => 'Description',
  'min_word_count' => '0',
  'help' => '',
  'node_options' =>
  array (
    'status' => true,
    'promote' => true,
    'sticky' => false,
    'revision' => false,
  ),
  'old_type' => 'fapi_reference',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'comment' => '2',
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => 0,
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
);
$content[fields]  = array (
  0 =>
  array (
    'label' => 'Code example',
    'field_name' => 'field_code_example',
    'type' => 'text',
    'widget_type' => 'text_textarea',
    'change' => 'Change basic information',
    'weight' => '-1',
    'rows' => '5',
    'description' => '',
    'default_value_widget' =>
    array (
      'field_code_example' =>
      array (
        0 =>
        array (
          'value' => '',
          '_error_element' => 'default_value_widget][field_code_example][0][value',
          'format' => 1,
        ),
      ),
    ),
    'default_value_php' => '',
    'required' => '0',
    'multiple' => '0',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' =>
    array (
      'value' =>
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
      ),
      'format' =>
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
      ),
    ),
    'default_value' =>
    array (
      0 =>
      array (
        'value' => '',
        '_error_element' => 'default_value_widget][field_code_example][0][value',
        'format' => 1,
      ),
    ),
    'display_settings' =>
    array (
      'label' =>
      array (
        'format' => 'above',
      ),
      'teaser' =>
      array (
        'format' => 'default',
      ),
      'full' =>
      array (
        'format' => 'default',
      ),
      4 =>
      array (
        'format' => 'default',
      ),
    ),
  ),
  1 =>
  array (
    'label' => 'Properties / Used by',
    'field_name' => 'field_properties',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_buttons',
    'change' => 'Change basic information',
    'weight' => '',
    'description' => '',
    'default_value_widget' =>
    array (
      'field_properties' =>
      array (
        0 =>
        array (
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_properties][nid][nid',
        ),
        'nid' =>
        array (
          'nid' =>
          array (
            '' => 1,
          ),
          '_error_element' => 'default_value_widget][field_properties][nid][nid',
        ),
      ),
    ),
    'default_value_php' => '',
    'required' => '0',
    'multiple' => '1',
    'referenceable_types' =>
    array (
      'fapi_reference' => true,
      0 => 1,
      'page' => false,
      'story' => false,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' =>
    array (
      'nid' =>
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
      ),
    ),
    'display_settings' =>
    array (
      'label' =>
      array (
        'format' => 'above',
      ),
      'teaser' =>
      array (
        'format' => 'default',
      ),
      'full' =>
      array (
        'format' => 'default',
      ),
      4 =>
      array (
        'format' => 'default',
      ),
    ),
  ),
);
*/