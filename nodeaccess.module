<?php

/**
 * Implementation of hook_help().
 */
function nodeaccess_help($section) {
  if ($section == 'admin/modules#description') {
    return t('Allows users to grant permissions to other users and roles.');
  }
  if (preg_match('|^node/\d+/grant|', $section)) {
    if (user_access('administer users')) {
      $message = t('You can set grants by roles and users.');
    }
    else {
      $message = t('You can set grants per users.');
    }
    $message .= t(' You need to check the Keep checkbox if you want to keep the user for granting. Users with Keep checkbox checked remain in the user table between searches. Note that user rights are additional to those coming from roles.');
  }
}


/**
 * Implementation of hook_menu().
 */
function nodeaccess_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array('path' => 'admin/settings/nodeaccess', 'title' => t('node grants'),
      'callback' => 'nodeaccess_admin', 'access' => user_access('grant node permissions'));
    $items[] = array('path' => 'admin/settings/nodeaccess/settings', 'title' => t('node grants'),
      'callback' => 'nodeaccess_admin', 'access' => user_access('grant node permissions'),
      'type' => MENU_DEFAULT_LOCAL_TASK);
    $items[] = array('path' => 'admin/settings/nodeaccess/terms', 'title' => t('terms'),
      'callback' => 'nodeaccess_admin_terms', 'access' => user_access('grant node permissions'),
      'weight' => 5, 'type' => MENU_LOCAL_TASK);
  }
  else {
    if (arg(0) == 'node' && is_numeric(arg(1))) {
      $node = node_load(array('nid' => arg(1)));
      $type = variable_get('nodeaccess-types', array());
      if ($node->nid && (user_access('grant node permissions') || $types[$node->type])) {
        $items[] = array('path' => 'node/'. $node->nid .'/grant', 'title' => t('grant'),
          'callback' => 'nodeaccess_page', 'callback arguments' => $node->nid,
          'access' => nodeaccess_access('grant', $node),
          'weight' => 5,
          'type' => MENU_LOCAL_TASK);
      }
    }
  }
  return $items;
}

/**
 * Implementation of hook_perm().
 */
function nodeacess_perm() {
  return array('grant node permissions', 'grant own node permissions');
}

/**
 * Implementation of hook_access().
 */
function nodeaccess_access($op, $node) {
  global $user;

  if ($op == 'grant') {
    if (user_access('grant node permissions') || (user_access('grant own node permissions') && ($user->uid == $node->uid))) {
      return TRUE;
    }
  }
}

/**
 * Menu callback. Draws the page.
 */
function nodeaccess_page($nid) {
  $edit = _nodeaccess_populate($nid);
  _nodeaccess_submit($nid, $edit);
  if (user_access('administer user')) {
    $roles_table = _nodeaccess_table($edit, 'rid', t('Role name'));
  }
  $search_form = _nodeaccess_search_form($edit['keys']);
  $users_table = _nodeaccess_table($edit, 'uid', t('User name'));
  $output = $roles_table . $search_form . $users_table ;
  if ($roles_table || $users_table) {
    $output .= '<div class="container-inline">';
    $output .= form_checkbox('', 'owndefault', 1, 0, t('Make these settings default for my content'));
    $output .= '</div>';
    $output .= form_submit(t('Save grants'));
  }
  print theme('page', form($output));
}

/**
 * Popupate $edit. Loads all roles, gathers uids from node_access and search,
 * and calculates the grants for the users.
 */
function _nodeaccess_populate($nid) {
  $edit = $_POST['edit'];
  // delete what is not kept
  if (is_array($edit['uid'])) {
    foreach ($edit['uid'] as $uid => $row) {
      if (!$row['keep']) {
        unset($edit['uid'][$uid]);
      }
    }
  }
  if (!$edit) {
    $edit = array();
    // load all roles
    $result = db_query("SELECT rid, name, na.* FROM {role} LEFT JOIN {node_access} na ON rid=gid AND realm='%s' AND nid=%d", 'nodeaccess_rid', $nid);
    while ($grant = db_fetch_object($result)) {
      $edit['rid'][$grant->rid] = array('name' => $grant->name, 'grant_view' => $grant->grant_view, 'grant_update' => $grant->grant_update, 'grant_delete' => $grant->grant_delete);
    }
    // load uids from node_access
    $result = db_query("SELECT uid, name FROM {node_access} na LEFT JOIN users ON uid=gid WHERE nid=%d AND realm='%s'", $nid, 'nodeaccess_uid');
    while ($account = db_fetch_object($result)) {
      $edit['uid'][$account->uid] = array('name' =>  $account->name, 'keep' => 1);
    }
  }
  // perform search
  if ($edit['keys'] && $edit['old_keys'] != $edit['keys']) {
    $sql = "SELECT uid, name FROM {users} WHERE LOWER(name) LIKE LOWER('%%%s%%')";
    $params = array(preg_replace('!\*+!', '%', $edit['keys']));
    if (is_array($edit['uid'])) {
      $sql .= ' AND uid NOT IN (%s)';
      $params[] = implode(',', array_keys($edit['uid']));
    }
    $result = db_query_range($sql, $params, 0, 15);
    while ($account = db_fetch_object($result)) {
      $edit['uid'][$account->uid] = array('name' =>  $account->name);
    }
  }
  // calculate node grants for users
  if (is_array($edit['uid'])) {
    foreach (array_keys($edit['uid']) as $uid) {
      if (!isset($edit['uid'][$uid]['grant_view'])) {
        foreach (array('grant_view', 'grant_update', 'grant_delete') as $grant_type) {
          $edit['uid'][$uid][$grant_type] = db_num_rows(db_query_range("SELECT * FROM {node_access} na LEFT JOIN {users_roles} r ON na.gid=r.rid WHERE nid=%d AND realm='%s' AND uid=%d AND %s=1", $nid, 'nodeaccess_rid', $uid, $grant_type, 0, 1)) || db_num_rows(db_query_range("SELECT * FROM {node_access} na WHERE nid=%d AND realm='%s' AND gid=%d AND %s=1", $nid, 'nodeaccess_uid', $uid, $grant_type, 0, 1));
        }
      }
    }
  }
  return $edit;
}

/**
 * Save to the database if necessary.
 */
function _nodeaccess_submit($nid, $edit) {
  global $user;

  switch ($_POST['op']) {
    case t('Save grants'):
      $grants = array();
      foreach (array('uid', 'rid') as $type) {
        if (is_array($edit[$type])) {
          foreach ($edit[$type] as $gid => $line) {
            $realm = 'nodeaccess_'. $type;
            $grants[$realm][$gid] = array($gid, $realm, $line['grant_view'], $line['grant_update'], $line['grant_delete']);
          }
        }
      }
      _nodeaccess_save_new($nid, $grants);
      drupal_set_message(t('Grants saved.'));
      break;
  }

  if ($edit['owndefault']) {
    user_save($user, array('nodeaccess_defaults' => $grants));
    _nodeaccess_save_default($grants, 'n.uid=%d', $user->uid);
    drupal_set_message(t('Saved these settings as default for my content'));
  }
}

function _nodeaccess_save_new($nid, $grants) {
  db_query("DELETE FROM {node_access} WHERE nid=%d AND realm IN ('%s')", $nid, implode("','", array_keys($grants)));
  foreach ($grants as $realm) {
    foreach ($realm as $grant) {
      $grant[] = $nid;
      db_query("INSERT INTO {node_access} (gid, realm, grant_view, grant_update, grant_delete, nid) VALUES (%d, '%s', %d, %d, %d, %d)", $grant);
    }
  }
}

/**
 * Save default values where no grant is present
 */
function _nodeaccess_save_default($grants, $where = '', $param = '', $join = '', $extra = '') {
  db_query("CREATE TEMPORARY TABLE nodeaccess_temp AS SELECT n.nid FROM {node} n  $join LEFT JOIN {node_access} na ON n.nid=na.nid $extra WHERE ". $where .' AND na.nid IS NULL', $param);
  foreach ($grants as $realm) {
    foreach ($realm as $grant) {
      db_query("INSERT INTO {node_access} (nid, gid, realm, grant_view, grant_update, grant_delete) SELECT nid, %d, '%s', %d, %d, %d FROM nodeaccess_temp", $grant);
    }
  }
  db_query('DROP TEMPORARY TABLE nodeaccess_temp');
}

/**
 * Draws search form.
 *
 */
function _nodeaccess_search_form($keys) {
  $output = ' <div class="search-form">';
  $box = '<div class="container-inline">';
  $prompt = t('Enter your keywords to search for users');
  $box .= form_hidden('old_keys', $keys);
  $box .= form_textfield('', 'keys', $keys, 40, 255);
  $box .= form_submit(t('Search'));
  $box .= '</div>';
  $output .= form_item($prompt, $box);
  $output .= '</div>';

  return $output;
}


function _nodeaccess_table($edit, $tablekey, $name) {
  $fields = $edit[$tablekey];
  if (empty($fields)) {
    return;
  }
  foreach ($fields as $key => $field) {
    $row = array(
      form_hidden($tablekey .']['. $key .'][name', $field['name']). $field['name'],
      form_checkbox('', $tablekey .']['. $key .'][grant_view', 1, $field['grant_view']),
      form_checkbox('', $tablekey .']['. $key .'][grant_update', 1, $field['grant_update']),
      form_checkbox('', $tablekey .']['. $key .'][grant_delete', 1, $field['grant_delete']));
    if ($tablekey == 'uid') {
      array_unshift($row, form_checkbox('', 'uid]['. $key .'][keep', 1, $field['keep']));
    }
    $rows[] = $row;
  }

  if (!empty($rows)) {
    $header = array($name, t('View'), t('Edit'), t('Delete'));
    if ($tablekey == 'uid') {
      array_unshift($header, t('Keep'));
    }
    tablesort_init($header);
    $table = theme('table', $header, $rows);
    return $table;
  }
}

function nodeaccess_node_grants($user, $op) {
  global $user;

  $roles = is_array($user->roles) ? array_keys($user->roles) : array(-1);
  return array('nodeaccess_rid' => $roles, 'nodeaccess_uid' => array($user->uid));
}

function nodeaccess_nodeapi(&$node, $op) {
  global $user;

  switch ($op) {
    case 'insert':
      $grants = $user->nodeaccess_defaults ? $user->nodeaccess_defaults : variable_get('nodeaccess_'. $node->type, array());
      _nodeaccess_save_new($node->nid, $grants);
      break;
    case 'delete':
      db_query('DELETE FROM {node_access} WHERE nid=%d', $node->nid);
      break;
  }
}

function nodeaccess_admin() {
  $edit = $_POST['edit'];
  if ($edit) {
    variable_set('nodeaccess-types', $edit['types']);
    foreach (node_list() as $type) {
      $grants = variable_get('nodeaccess_'. $type, array());
      $nids = array();
      $result = db_query("SELECT na.nid FROM {node_access} na LEFT JOIN {node} n ON na.nid=n.nid WHERE type='%s'", $type);
      while ($node = db_fetch_object($result)) {
        $nids[] = $node->nid;
      }
      if (!empty($nids) && isset($grants['rid'])) {
        foreach ($grants['rid'] as $grant) {
          $grant[] = implode(',', $nids);
          db_query("DELETE FROM {node_access} WHERE gid=%d AND realm='%s' AND grant_view=%d AND grant_update=%d AND grant_delete=%d AND nid IN (%s)", $grant);
        }
      }
      $grants = array();
      foreach ($edit[$type] as $gid => $line) {
        $grants['rid'][$gid] = array($gid, 'nodeaccess_rid', 'grant_view' => $line['grant_view'], 'grant_update' => $line['grant_update'], 'grant_delete' => $line['grant_delete']);
      }
      variable_set('nodeaccess_'. $type, $grants);
      _nodeaccess_save_default($grants, "n.type='%s'", $type);
    }
  }
  $result = db_query('SELECT rid, name FROM {role} ORDER BY name');
  while ($role = db_fetch_object($result)) {
    $roles[$role->rid] = $role->name;
  }
  foreach (node_list() as $type) {
    $group = '<div class="container-inline">';
    $types = variable_get('nodeaccess-types', array());
    $group .= form_checkbox(t('Show grant tab for this node type'), 'types]['. $type, 1, $types[$type]);
    $group .= '</div>';
    $grants = variable_get('nodeaccess_'. $type, array());
    foreach ($roles as $rid=>$name) {
      $edit[$type][$rid] = array_merge(array('name'=> $name), (array)($grants['rid'][$rid]));
    }
    $group .= _nodeaccess_table($edit, $type, t('Role name'));
    $output .= form_group(node_invoke($type, 'node_name'), $group);
  }
  $output .= form_submit(t('Save settings'));
  print theme('page', form($output));
}

function nodeaccess_admin_terms() {
  $edit = $_POST['edit'];
  if ($edit) {
    variable_set('nodeaccess_term_grants', $edit['tid']);
    db_query("DELETE FROM {node_access_term}");
    foreach($edit['tid'] as $tid => $row) {
      foreach ($row as $rid => $grant) {
        if ($grant) {
          db_query("INSERT INTO {node_access_term} (tid, rid) VALUES (%d, %d)", $tid, $rid);
        }
      }
    }
  }
  $term_grants = variable_get('nodeaccess_term_grants', array());
  $result = db_query('SELECT rid, name FROM {role} ORDER BY name');
  while ($role = db_fetch_object($result)) {
    $roles[$role->rid] = $role->name;
  }
  $rows = array();
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vocabulary) {
    $rows[] = array(array('data' => $vocabulary->name, 'colspan' => count($roles)+1, 'class' => 'grantvocab'));
    $tree = taxonomy_get_tree($vocabulary->vid);
    foreach ($tree as $term) {
      $row = array(_taxonomy_depth($term->depth) . $term->name);
      foreach (array_keys($roles) as $rid) {
        $row[] = form_checkbox('', 'tid]['. $term->tid .']['. $rid, 1, $term_grants[$term->tid][$rid] || empty($term_grants));
      }
      $rows[] = $row;
    }
  }
  array_unshift($roles, 'Term name');
  $output = theme('table', $roles, $rows);
  $output .= form_submit(t('Save settings'));
  print theme('page', form($output));
}

function nodeaccess_db_rewrite_sql($query, $primary_table, $primary_field) {
  global $user;

  $term_grants = variable_get('nodeaccess_term_grants', array());
  $roles = is_array($user->roles) ? array_keys($user->roles) : array(-1);
  if ($primary_field == 'nid' && !empty($term_grants)) {
    $return['join'] = "LEFT JOIN {term_node} na_tn ON $primary_table.nid=na_tn.nid LEFT JOIN {node_access_term} na_na ON na_tn.tid=na_na.tid AND na_na.rid IN (". implode(',', $roles) .")";
    $return['where'] ="na_na.tid IS NOT NULL";
    $return['distinct'] = 1;
    return $return;
  }
}

?>
