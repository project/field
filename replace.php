<?php
   function file_replace ($search, $replace, $filename) {
       if (file_exists($filename)) {
           $cnt = file_get_contents($filename);
               $cnt = preg_replace($search, $replace, $cnt);
               return file_put_contents($filename, $cnt);
           return true;
       }
       return false;
   }
   
   function dir_replace ($search, $replace, $dirname, $recursive = true) {
       $dir = opendir($dirname);
       while ($file = readdir($dir)) {
           if ($file != '.' && $file != '..') {
               if (is_dir($dirname.'/'.$file)) {
                   if ($recursive) {
                       dir_replace($search, $replace, $dirname.'/'.$file);
                   }
               } else {
                   file_replace($search, $replace, $dirname.'/'.$file);
               }
           }
       }
   }
if(!function_exists('file_put_contents')) {
  function file_put_contents($filename, $data, $file_append = false) {
   $fp = fopen($filename, (!$file_append ? 'w+' : 'a+'));
   if(!$fp) {
     trigger_error('file_put_contents cannot write in file.', E_USER_ERROR);
     return;
   }
   fputs($fp, $data);
   fclose($fp);
  }
}
dir_replace("/print theme\('page', +(.*)\);/Us", 'return \1;', './modules', FALSE);
?>
