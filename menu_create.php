<?php

require_once('./includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
define('MENU_IS_ROOT', 0x0001);
define('MENU_VISIBLE_IN_TREE', 0x0002);
define('MENU_VISIBLE_IN_BREADCRUMB', 0x0004);
define('MENU_VISIBLE_IF_HAS_CHILDREN', 0x0008);
define('MENU_MODIFIABLE_BY_ADMIN', 0x0010);
define('MENU_MODIFIED_BY_ADMIN', 0x0020);
define('MENU_CREATED_BY_ADMIN', 0x0040);
define('MENU_IS_LOCAL_TASK', 0x0080);
define('MENU_EXPANDED', 0x0100);
define('MENU_LINKS_TO_PARENT', 0x0200);
define('MENU_NORMAL_ITEM', MENU_VISIBLE_IN_TREE | MENU_VISIBLE_IN_BREADCRUMB | MENU_MODIFIABLE_BY_ADMIN);
define('MENU_ITEM_GROUPING', MENU_VISIBLE_IF_HAS_CHILDREN | MENU_VISIBLE_IN_BREADCRUMB | MENU_MODIFIABLE_BY_ADMIN);
define('MENU_CALLBACK', MENU_VISIBLE_IN_BREADCRUMB);
define('MENU_SUGGESTED_ITEM', MENU_MODIFIABLE_BY_ADMIN | MENU_VISIBLE_IN_BREADCRUMB);
define('MENU_LOCAL_TASK', MENU_IS_LOCAL_TASK);
define('MENU_DEFAULT_LOCAL_TASK', MENU_IS_LOCAL_TASK | MENU_LINKS_TO_PARENT);
define('MENU_CUSTOM_ITEM', MENU_VISIBLE_IN_TREE | MENU_VISIBLE_IN_BREADCRUMB | MENU_CREATED_BY_ADMIN | MENU_MODIFIABLE_BY_ADMIN);
define('MENU_CUSTOM_MENU', MENU_IS_ROOT | MENU_VISIBLE_IN_TREE | MENU_CREATED_BY_ADMIN | MENU_MODIFIABLE_BY_ADMIN);


db_query('DELETE FROM {menu_new}');
$menu = node_menu() + user_menu(); // module_invoke_all('menu')
$menu['admin'] = array('title' => t('administer'), 'access' => 'user_access', 'access arguments' => array('access administration pages'), 'callback' => 'system_main_admin_page', 'weight' => 9);

foreach (module_implements('menu_alter') as $module) {
  $function = $module .'_menu_alter';
  $function($menu);
}
$mid = -1;
foreach ($menu as $path => $item) {
  $menu[$path]['mid'] = $mid--; // negative mid for programmed items
  $parts = explode('/', $path, 6);
  $number_parts = count($parts);
  $menu[$path]['_parts'] = $parts;
  $menu[$path]['_number_parts'] = $number_parts;
  // TODO: move $item['path'] to the menu key
  $sort[$path] = $number_parts . sprintf('%05d', isset($item['weight']) ? $item['weight'] : 0) . $path;
}
array_multisort($sort, $menu);
$next = array();
foreach ($menu as $path => $item) {
  $parts = $item['_parts'];
  $number_parts = $item['_number_parts'];
  // we store the highest index of parts here to save some work in the weight
  // calculation loop
  $slashes = $number_parts - 1;
  if (strpos($path, '%') === FALSE) {
    $db_weight = (1 << $number_parts) - 1;
  }
  else {
    $db_weight = 0;
    foreach ($parts as $k => $part) {
      // ($part != '%') is the bit we want and we shift it to its place
      // by shifting to left by ($slashes - $k) bits
      $db_weight |=  ($part != '%') << ($slashes - $k);
    }
  }
  $breadcrumb = $parents = array();
  unset($pid);
  while ($path) {
    $path = substr($path, 0, strrpos($path, '/'));
    if (isset($menu[$path])) {
      $current_item = $menu[$path];
      if (isset($item['access']) && isset($current_item['access'])) {
        $item['access'] = $current_item['access'];
        $item['access argument'] = $current_item['access argument'];
      }
      if (!isset($item['callback']) && isset($current_item['callback'])) {
        $item['callback'] = $current_item['callback'];
        $item['callback argument'] = $current_item['callback argument'];
      }
      if (isset($current_item['title'])) {
        $breadcrumb[] = $current_item['title'];
        if (!isset($item['title'])) {
          $item['title'] = $current_item['title'];
        }
      }
      if (!isset($pid)) {
        $pid = $current_item['mid'];
        $parent_path = $path;
      }
      $parents[] = $current_item['mid'];
    }
  }
  $depth = count($parents);
  $parents[] = 0;
  $breadcrumb = array_reverse($breadcrumb);
  $parents = array_reverse($parents);
  $path = implode('/', $parts);
  $prefix = $depth ? $menu[$parent_path]['_prefix'] : '';
  if (!isset($next[$prefix])) {
    $next[$prefix] = int2vancode(0);
  }
  $vancode = $prefix . int2vancode($next[$prefix]++);
  $menu[$path]['_prefix'] = $vancode .'.';
  db_query("INSERT INTO {menu_new} (mid, pid, path, title, access, access_arguments, callback, callback_arguments, weight, type, parents, breadcrumb, vancode, parts, db_weight) VALUES (%d, %d, '%s', '%s', '%s', '%s', '%s', '%s', %d, %d, '%s', '%s', '%s', %d, %d)",
  $item['mid'], $pid,
  $path, $item['title'],
  is_bool($item['access']) ? intval($item['access']) : $item['access'],
  isset($item['access arguments']) ? serialize($item['access arguments']) : '', $item['callback'],
  isset($item['callback arguments']) ? serialize($item['callback arguments']) : '',
  isset($item['weight']) ? $item['weight'] : 0,
  isset($item['type']) ? $item['type'] : MENU_NORMAL_ITEM,
  implode(',' , $parents),
  serialize($breadcrumb), // TODO: theme this right here
  $vancode .'+', // we use plus instead of slash in original vancode because + is before .
  $number_parts, $db_weight);
}


