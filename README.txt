I have used PHP4 and the sqlite extension from PECL.

1) Apply database.inc.patch.txt in includes. This is in http://drupal.org/node/13020
2) As a workaround of SQLite bug #523 (http://www.sqlite.org/cvstrac/tktview?tn=523) apply taxonomy_module.txt
3) Copy database.sqlite.inc to includes.
4) gunzip and copy the database to a directory which is writeable by the web server. Also 
the database should be writeable. It is advised to rename it to something which starts 
with .ht , 'cos most Apaches are configured to block anything named .ht*.

I have this in conf.php:

$db_url = "sqlite://database/sqlite/.ht.sqlite";

ALTER TABLE is not yet implemented. Feel free to contact me:

chx mail tvnet hu
   @    .     .
