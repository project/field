<?php
define('NEEDS_PROCESSING', 0);
define('PROCESSING', 1);
define('PROCESSED', 2);

$hooks['comment']['nodeapi'] = array();
$hooks['node']['nodeapi'] = array(
//*
  'before' => array('poll')
//*/
);
$hooks['taxonomy']['nodeapi']['after'] = array('comment');
$hooks['forum']['nodeapi']['before'] = array('node');
$hooks['poll']['nodeapi'] = array('before' => array('forum'), 'after' => array('taxonomy'));
$hooks['users']['nodeapi'] = array();

$queue = $current_hook = array();
foreach ($hooks as $module => $element) {
  $current_hook[$module] = $element['nodeapi'];
}

$state = array();
foreach ($current_hook as $start => $hook) {
  if (isset($hook['before'])) {
    $state[$start] = NEEDS_PROCESSING;
    // edges pointing out from this vertex
    foreach ($hook['before'] as $end) {
      $adj[$start][$end] = 1;
      if (!isset($adj[$end])) {
        $adj[$end] = array();
        $state[$end] = NEEDS_PROCESSING;
      }
    }
  }
  if (isset($hook['after'])) {
    $state[$start] = NEEDS_PROCESSING;
    // edges pointing to this vertex
    if (!isset($adj[$start])) {
      $adj[$start] = array();
    }
    foreach ($hook['after'] as $end) {
      $adj[$end][$start] = 1;
    }
  }
}

function dfs(&$state, $adj, $key) {
  static $ordered = array(), $error = FALSE;
  if (!$error) {
    $state[$key] = PROCESSING;
    foreach (array_keys($adj[$key]) as $new_key) {
      switch ($state[$new_key]) {
        case NEEDS_PROCESSING:
          dfs($state, $adj, $new_key);
          break;
        case PROCESSING:
          $error = TRUE;
          break 2;
      }
    }
    $state[$key] = PROCESSED;
    // instead of array_unshifting all the time we will reverse later
    $ordered[] = $key;
  }
  return $error ? array() : $ordered;
}

$ordered = array();
foreach (array_keys($adj) as $key) {
  if ($state[$key] == NEEDS_PROCESSING) {
    $ordered = dfs($state, $adj, $key);
  }
}
if ($ordered) {
  echo "done\n";
  $ordered = array_merge(array_reverse($ordered), array_diff(array_keys($current_hook), array_keys($state)));
}
else {
  echo "error\n";
}
var_dump($ordered);