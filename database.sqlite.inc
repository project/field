<?php
// $Id

/**
 * @file
 * Database interface code for SQLite embedded database engine.
 */

/**
 * @ingroup database
 * @{
 */

/**
 * Initialize a database connection.
 */
function db_connect($url) {
  $url=substr($url,9);
  
  $connection = sqlite_open($url) or die('unable to select database');
  sqlite_create_function($connection,'concat','_sqlite_concat');

  return $connection;
}

/**
 * A user defined function to replace the CONCAT in MYSQL
 *
 * sqlite_udf_encode_binary in the PHP manual says sg. about first byte should not be 0x01 and there should not be 0x00 chars. 
 * But we can safely assume only UTF-8 strings will be CONCATed, no problem with that.
 * @param ...
 * A variable number of arguments which are concatenated
*/ 

function _sqlite_concat() {
  $a = func_get_args();
  return implode('', $a);
}

/**
 * Runs a basic query in the active database.
 *
 * User-supplied arguments to the query should be passed in as separate parameters
 * so that they can be properly escaped to avoid SQL injection attacks.
 *
 * @param $query
 *   A string containing an SQL query.
 * @param ...
 *   A variable number of arguments which are substituted into the query using
 *   printf() syntax.
 * @return
 *   A database query result resource, or FALSE if the query was not executed
 *   correctly.
 */
function db_query($query) {
  $args = func_get_args();

  $query = db_prefix_tables($query);
  if (count($args) > 1) {
    if(is_array($args[1])){
      $args1 = array_map('db_escape_string', $args[1]);
      $nargs = array_merge(array($query), $args1);
    }
    else {
      $nargs = array_map('db_escape_string', $args);
      $nargs[0] = $query;
    }
    return _db_query(call_user_func_array('sprintf', $nargs));
  }
  else {
    return _db_query($query);
  }
}

/**
 * Debugging version of db_query().
 *
 * Echoes the query to the browser.
 */
function db_queryd($query) {
  $args = func_get_args();
  $query = db_prefix_tables($query);
  if (count($args) > 1) {
    if(is_array($args[1])){
      $args1 = array_map('db_escape_string', $args[1]);
      $nargs = array_merge(array($query), $args1);
    }
    else {
      $nargs = array_map('db_escape_string', $args);
      $nargs[0] = $query;
    }
    return _db_query(call_user_func_array('sprintf', $nargs), 1);
  }
  else {
    return _db_query($query, 1);
  }
}

function _sqlite_expand($matches) {
  global $active_db;
  $table = $matches[1];
  $real_table = $table;
  if (preg_match('/(FROM |JOIN |, ?)(\w+) '.$table.'[ ,]/',_sqlite_get_query(),$m))
    $real_table=$m[2];
  $array=sqlite_array_query($active_db,"SELECT sql FROM sqlite_master WHERE type='table' AND sql LIKE 'CREATE TABLE $real_table %'");
  $define=explode("\n",$array[0]['sql']);
  $replace=array();
  foreach ($define as $inner_field) {
    if ($inner_field{3}==' ') {
      $inner_field=substr($inner_field,4);
      $inner_field=substr($inner_field,0,strpos($inner_field,' '));
      $replace[]=$table.'.'.$inner_field.' AS '.$inner_field;
    }
  }
  return implode(', ',$replace);
}

function _sqlite_get_query() {
  return _sqlite_set_query();
}

function _sqlite_set_query($query=NULL) {
  static $query_cache;
  if ($query) $query_cache = $query;
  return $query_cache;
}

/**
 * Helper function for db_query().
 */
function _db_query($query, $debug = 0) {
  global $active_db;
  global $queries;

  if (variable_get('dev_query', 0)) {
    list($usec, $sec) = explode(' ', microtime());
    $timer = (float)$usec + (float)$sec;
  }
  
  $old_query = $query;
  $query=preg_replace('/^SELECT COUNT\(DISTINCT\(([^.)]+\.)?([^\)]+)\)\)( AS [^ ]+)? FROM(.+)$/i','SELECT COUNT(\2)\3 FROM (SELECT DISTINCT(\1\2) FROM\4)',$query);
  if (preg_match('/^SELECT(.+)FROM(.+)$/',$query,$select) && ($old_query==$query)) {
    _sqlite_set_query($query);
    $select[1]=preg_replace_callback('/([^( ,]+)\.\*/','_sqlite_expand',$select[1]);
    $select[1]=preg_replace('/[.(]([^.* ,)]+)\)?(?=[ ,])(?! AS)/i','\0 AS \1',$select[1]); // could be  (?! +AS) but this is faster and seems to work
    $query='SELECT'.$select[1].'FROM'.$select[2];
  }

  //echo $query.'<br>';
  if (!$result = sqlite_query($active_db,$query)) {
//    echo _sqlite_get_query().'<br>'.$query;
  }
  
  if (variable_get('dev_query', 0)) {
    list($usec, $sec) = explode(' ', microtime());
    $stop = (float)$usec + (float)$sec;
    $diff = $stop - $timer;
    $queries[] = array($query, $diff);
  }

  if ($debug) {
    print '<p>query: '. $query .'<br />error:'. sqlite_last_error($active_db) .'</p>';
  }
  if (!($error_code=sqlite_last_error($active_db))) {
    return $result;
  }
  else {
    trigger_error(sqlite_error_string($error_code) ."\nquery: ". htmlspecialchars($query), E_USER_ERROR);
  }
}

/**
 * Fetch one result row from the previous query as an object.
 *
 * @param $result
 *   A database query result resource, as returned from db_query().
 * @return
 *   An object representing the next row of the result. The attributes of this
 *   object are the table fields selected by the query.
 */
function db_fetch_object($result) {
  global $active_db,$last_query;
  
  if ($result && sqlite_has_more($result)) {
    return (object)sqlite_fetch_array($result,SQLITE_ASSOC);
  }
}

/**
 * Fetch one result row from the previous query as an array.
 *
 * @param $result
 *   A database query result resource, as returned from db_query().
 * @return
 *   An associative array representing the next row of the result. The keys of
 *   this object are the names of the table fields selected by the query, and
 *   the values are the field values for this result row.
 */
function db_fetch_array($result) {
  if ($result  && sqlite_has_more($result)) {
    return sqlite_fetch_array($result, SQLITE_ASSOC);
  }
}

/**
 * Determine how many result rows were found by the preceding query.
 *
 * @param $result
 *   A database query result resource, as returned from db_query().
 * @return
 *   The number of result rows.
 */
function db_num_rows($result) {
  if ($result) {
    return sqlite_num_rows($result);
  }
}

/**
 * Return an individual result field from the previous query.
 *
 * Only use this function if exactly one field is being selected; otherwise,
 * use db_fetch_object() or db_fetch_array().
 *
 * @param $result
 *   A database query result resource, as returned from db_query().
 * @param $row
 *   The index of the row whose result is needed.
 * @return
 *   The resulting field.
 */
function db_result($result, $row = 0) {
  if ($result && sqlite_num_rows($result) > $row) {
    sqlite_seek($result,$row);
    return sqlite_fetch_single($result);
  }
}

/**
 * Determine whether the previous query caused an error.
 */
function db_error() {
  global $active_db;
  return sqlite_last_error($active_db);
}

/**
 * Return a new unique ID in the given sequence.
 *
 * For compatibility reasons, Drupal does not use auto-numbered fields in its
 * database tables. Instead, this function is used to return a new unique ID
 * of the type requested. If necessary, a new sequence with the given name
 * will be created.
 */
function db_next_id($name) {
  $name = db_prefix_tables($name);
  db_query('BEGIN');
  $id = db_result(db_query("SELECT id FROM {sequences} WHERE name = '%s'", $name)) + 1;
  db_query("REPLACE INTO {sequences} VALUES ('%s', %d)", $name, $id);
  db_query('COMMIT');

  return $id;
}

/**
 * Determine the number of rows changed by the preceding query.
 */
function db_affected_rows() {
  global $active_db;
  return sqlite_changes($active_db);
}

/**
 * Runs a limited-range query in the active database.
 *
 * Use this as a substitute for db_query() when a subset of the query is to be
 * returned.
 * User-supplied arguments to the query should be passed in as separate parameters
 * so that they can be properly escaped to avoid SQL injection attacks.
 *
 * @param $query
 *   A string containing an SQL query.
 * @param ...
 *   A variable number of arguments which are substituted into the query using
 *   printf() syntax.
 * @param $from
 *   The first result row to return.
 * @param $count
 *   The maximum number of result rows to return.
 * @return
 *   A database query result resource, or FALSE if the query was not executed
 *   correctly.
 */
function db_query_range($query) {
  $args = func_get_args();
  $count = array_pop($args);
  $from = array_pop($args);
  if (count(func_get_args()) > 3) {
    $args = array_map('db_escape_string', $args);
    $query = db_prefix_tables($query);
    $args[0] = $query;
    $query = call_user_func_array('sprintf', $args);
  }
  else {
    $query = func_get_arg(0);
    $query = db_prefix_tables($query);
  }
  $query .= ' LIMIT '. $count .' OFFSET  '.$from ;
  return _db_query($query);
}

/**
 * Returns a properly formatted Binary Large OBject value.
 *
 * @param $data
 *   Data to encode.
 * @return
 *  Encoded data.
 */
function db_encode_blob($data) {
  return $data;
}

/**
 * Returns text from a Binary Large OBject value.
 *
 * @param $data
 *   Data to decode.
 * @return
 *  Decoded data.
 */
function db_decode_blob($data) {
  return $data;
}

 /**
 * Prepare user input for use in a database query, preventing SQL injection attacks.
 */
function db_escape_string($text) {
  return sqlite_escape_string($text);
}

/**
 * @} End of "ingroup database".
 */

?>
